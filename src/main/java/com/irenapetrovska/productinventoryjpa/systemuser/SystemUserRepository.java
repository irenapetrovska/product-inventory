package com.irenapetrovska.productinventoryjpa.systemuser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SystemUserRepository extends JpaRepository<SystemUser, Long> {

    @Query("select group from Group group where group.groupName = :name")
    Optional<Group> findGroupByName(String name);
}
