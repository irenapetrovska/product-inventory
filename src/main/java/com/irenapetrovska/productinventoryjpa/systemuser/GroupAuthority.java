package com.irenapetrovska.productinventoryjpa.systemuser;

import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "group_authorities")
@NoArgsConstructor
@ToString
public class GroupAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public  Integer id;
    public String authority;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    @ToString.Exclude
    public Group group;

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupAuthority authority = (GroupAuthority) o;
        return Objects.equals(id, authority.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
