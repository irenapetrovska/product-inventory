package com.irenapetrovska.productinventoryjpa.systemuser;

import com.irenapetrovska.productinventoryjpa.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@RequiredArgsConstructor
public class SystemUserService {

    private final SystemUserRepository systemUserRepository;
    private final PasswordEncoder encoder;

    @Transactional
    public SystemUserDTO register(@Valid final SystemUserRegisterRequest request){
        var user = request.toEntity();
        var group = systemUserRepository.findGroupByName("USER_GROUP")
                .orElseThrow(() -> new ResourceNotFoundException("User group is not found"));
        user.addGroup(group);
        user.password = encoder.encode(request.password);
        return systemUserRepository.save(user).toDTO();
    }
}
