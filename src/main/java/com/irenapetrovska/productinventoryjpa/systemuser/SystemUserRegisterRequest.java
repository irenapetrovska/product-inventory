package com.irenapetrovska.productinventoryjpa.systemuser;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public class SystemUserRegisterRequest {

    @NotBlank(message = "First Name is required")
    public String firstName;
    @NotBlank(message = "Last Name is required")
    public String lastName;
    @NotBlank(message = "Username is required")
    public String username;
    @NotBlank(message = "Password is required")
    public String password;
    @NotBlank(message = "Email is required")
    @Email(message = "Please insert valid email format")
    public String email;
    public String address;

    public SystemUser toEntity(){
        return  new SystemUser(username,firstName,lastName,email,address, Instant.now(),true);
    }
}
