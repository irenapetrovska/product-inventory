package com.irenapetrovska.productinventoryjpa.systemuser;

import lombok.AllArgsConstructor;

import java.time.Instant;

@AllArgsConstructor
public class SystemUserDTO {

    public String firstName;
    public String lastName;
    public String username;
    public String email;
    public String address;


}
