package com.irenapetrovska.productinventoryjpa.utils;

public class VoidDTO {

    public boolean success;

    public VoidDTO(boolean success) {
        this.success = success;
    }
}
