package com.irenapetrovska.productinventoryjpa.confg;

import com.irenapetrovska.productinventoryjpa.exception.ResourceNotFoundException;
import com.irenapetrovska.productinventoryjpa.utils.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;


@ControllerAdvice
@Component
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handle(ResourceNotFoundException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDTO(exception.getMessage());
    }
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handle(ConstraintViolationException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDTO(exception.getMessage());
    }
}
