package com.irenapetrovska.productinventoryjpa.inventory;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class InventoryDTO {

    public final Integer id;
    public final String name;
    public final String location;

    public InventoryDTO(Integer id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }
}
