package com.irenapetrovska.productinventoryjpa.inventory.product;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @Size( max = 100, message = "Name must be max of 100 characters")
    public String name;
    @Size( max = 50, message = "Type must be max of 50 characters")
    @NotBlank(message = "Type is required")
    public String type;
    @NotNull(message = "Price is required")
    public BigDecimal price;
    @Size( max = 1000, message = "Description must be max of 1000 characters")
    public String description;
}
