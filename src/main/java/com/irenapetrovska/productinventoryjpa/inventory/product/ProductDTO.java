package com.irenapetrovska.productinventoryjpa.inventory.product;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

@EqualsAndHashCode
@ToString
public class ProductDTO {

    public final Integer id;
    public final String name;
    public final String type;
    public final BigDecimal price;
    public final String description;

    public ProductDTO(Integer id, String name, String type, BigDecimal price, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
    }
}
