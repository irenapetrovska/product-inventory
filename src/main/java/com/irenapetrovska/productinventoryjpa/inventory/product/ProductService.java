package com.irenapetrovska.productinventoryjpa.inventory.product;

import com.irenapetrovska.productinventoryjpa.exception.ResourceNotFoundException;
import com.irenapetrovska.productinventoryjpa.inventory.InventoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@Validated
public class ProductService {

    private InventoryRepository inventoryRepository;
    private ProductRepository productRepository;

    public ProductService(InventoryRepository inventoryRepository, ProductRepository productRepository) {
        this.inventoryRepository = inventoryRepository;
        this.productRepository = productRepository;
    }

    @Transactional
    public ProductDTO create(final Integer inventoryId, @Valid final ProductRequest request){
        var inventory = inventoryRepository.findById(inventoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID "+inventoryId+ " is not found"));
        var product= new Product(request.name,request.type,request.price,request.description);
        product.inventory=inventory;
        return  productRepository.save(product).toDTO();
    }
    
    @Transactional
    public ProductDTO update(final Integer productId,  @Valid final ProductRequest request){
        var product = productRepository.findById(productId)
               .orElseThrow(() -> new ResourceNotFoundException("Product with ID "+productId+ " is not found"));
        product.name = request.name;
        product.type = request.type;
        product.price = request.price;
        product.description = request.description;
        return productRepository.save(product).toDTO();
    }
    @Transactional
    public void delete(final Integer productId){
        var product = productRepository.findById(productId)
               .orElseThrow(() -> new ResourceNotFoundException("Product with ID "+productId+" is not found"));
        productRepository.delete(product);
    }



}
