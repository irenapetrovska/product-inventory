package com.irenapetrovska.productinventoryjpa.inventory.product;

import com.irenapetrovska.productinventoryjpa.utils.VoidDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
public class ProductResource {

    private ProductService productService;
    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDTO create(@RequestParam("inventoryId") final Integer inventoryId,
                             @RequestBody final ProductRequest request){
        return productService.create(inventoryId,request);
    }

    @PutMapping(path = "/{productId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDTO update(@PathVariable("productId") final Integer productId,
                             @RequestBody final ProductRequest request){
        return productService.update(productId,request);
    }

    @DeleteMapping(path = "/{productId}",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("productId") final Integer productId){
        productService.delete(productId);
        return  ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }
}
