package com.irenapetrovska.productinventoryjpa.inventory;

import com.irenapetrovska.productinventoryjpa.inventory.product.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventories")
public class InventoryResource {

    private InventoryService inventoryService;
    public InventoryResource(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO create(@RequestBody final InventoryRequest request){
        return inventoryService.create(request);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<InventoryDTO> findPage(
            @RequestParam(value = "location", required = false) final String location,
            @RequestParam(value = "name", required = false) final String name, Pageable pageable){
        return inventoryService.findPage(new InventorySearchRequest(name,location,pageable));
    }

    @GetMapping(path = "/find-all",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<InventoryDTO> findAll(){
        return inventoryService.findAll();
    }

    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/find-by-name/{name}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO findByName(@PathVariable("name") final String name){
        return inventoryService.findByNameNative(name);
    }
    @PutMapping(path = "/{inventoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO update(@PathVariable("inventoryId") final Integer inventoryId, @RequestBody final InventoryRequest request){
        return inventoryService.update(inventoryId,request);
    }
    @DeleteMapping(path = "/{inventoryId}",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> delete(@PathVariable("inventoryId") final Integer inventoryId){
        return inventoryService.delete(inventoryId);
    }

    @GetMapping(path = "/{inventoryId}/products", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ProductDTO> findAllProducts(@PathVariable("inventoryId") final Integer inventoryId){
        return inventoryService.findAllProducts(inventoryId);
    }

    @GetMapping(path = "/{inventoryId}/products/find-by-type/{type}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ProductDTO> findProductsByType(@PathVariable("inventoryId") final Integer inventoryId,
                                                @PathVariable("type") final String type){
        return inventoryService.findProductsByType(type,inventoryId);
    }


}
