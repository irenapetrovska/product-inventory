package com.irenapetrovska.productinventoryjpa.inventory;

import com.irenapetrovska.productinventoryjpa.exception.ResourceNotFoundException;
import com.irenapetrovska.productinventoryjpa.inventory.product.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InventoryService {

    private InventoryRepository repository;

    public InventoryService(InventoryRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public InventoryDTO create(final InventoryRequest request){
        var inventory= new Inventory(request.name, request.location);
        var createdInventory = repository.save(inventory);
        return createdInventory.toDTO();
    }

    public InventoryDTO findByNameNative(final String name){
       var foundInventory = repository.findFirstByNameNative(name)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with name "+name+" is not found."));
       return  foundInventory.toDTO();
    }

    public List<InventoryDTO> findAll(){
        return repository.findAll().stream()
                .map(inventory -> inventory.toDTO())
                .collect(Collectors.toList());
    }

    public List<ProductDTO> findAllProducts(final Integer inventoryId){
        return repository.findAllProducts(inventoryId).stream()
                .map(product -> product.toDTO())
                .collect(Collectors.toList());
    }
    public List<ProductDTO> findProductsByType(final String type, final Integer inventoryId){
        return repository.findAllProductsByType(type,inventoryId).stream()
                .map(product -> product.toDTO())
                .collect(Collectors.toList());
    }
    @Transactional
    public InventoryDTO update(final Integer inventoryId, final InventoryRequest request){
        var inventory = repository.findById(inventoryId)
               .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID "+inventoryId+ " is not found"));
        inventory.name = request.name;
        inventory.location = request.location;
        return repository.save(inventory).toDTO();
    }
    public ResponseEntity<HttpStatus> delete(final Integer inventoryId){
        var inventory = repository.findById(inventoryId)
               .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID "+inventoryId+ " is not found"));
        repository.delete(inventory);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

    public Page<InventoryDTO> findPage(final InventorySearchRequest request){
//        return request.generateSpecification().map(
//                specification -> repository.findAll(specification, request.pageable))
//                .orElseGet( () ->  repository.findAll(request.pageable))
//                .map(inventory -> inventory.toDTO());
        return  repository.findAll(request.generateSpecification(), request.pageable)
                .map(inventory -> inventory.toDTO());
    }


}
