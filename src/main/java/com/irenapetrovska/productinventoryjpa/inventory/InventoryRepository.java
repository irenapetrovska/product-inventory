package com.irenapetrovska.productinventoryjpa.inventory;

import com.irenapetrovska.productinventoryjpa.inventory.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;


import java.util.List;
import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, Integer>, JpaSpecificationExecutor<Inventory> {

    @Query("select product from Product product where product.inventory.id = :inventoryId")
    List<Product> findAllProducts(Integer inventoryId);

    @Query("select product from Product product inner join product.inventory inventory where product.type = :type and inventory.id = :inventoryId")
    List<Product> findAllProductsByType(String type, Integer inventoryId);

    @Query(nativeQuery = true, value="select * from inventory where name = :name limit 1")
    Optional<Inventory> findFirstByNameNative(String name);

    @Query(nativeQuery = true,
            value = "select * from inventory " +
            "where lower (name) = coalesce(lower(CAST(:name as VARCHAR )),lower (name))" +
            " and lower (location) = coalesce (lower (CAST (:location as VARCHAR )),lower (location))",
            countQuery = "select count(id) from inventory"+
            " where lower (name) = coalesce(lower( CAST(:name as VARCHAR )), lower (name))" +
            " and lower (location) = coalesce ( lower (CAST (:location as VARCHAR )),lower (location))" )
    Page<Inventory> findPage(final String location, final String name, Pageable pageable);


}
