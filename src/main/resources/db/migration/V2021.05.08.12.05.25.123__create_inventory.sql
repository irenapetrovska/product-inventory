CREATE TABLE inventory
(
    id serial NOT NULL ,
    name character varying(100),
    location character varying(50),
    CONSTRAINT inventory_pkey PRIMARY KEY (id)
);