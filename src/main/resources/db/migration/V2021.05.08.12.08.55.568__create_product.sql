CREATE TABLE product
(
    id serial NOT NULL,
    type character varying(50)  NOT NULL,
    price numeric(6,2) NOT NULL,
    description character varying(1000) ,
    inventory_id integer,
    name character varying(100) ,
    CONSTRAINT product_pkey PRIMARY KEY (id),
    CONSTRAINT fk_product_inventory_id FOREIGN KEY (inventory_id)
        REFERENCES public.inventory (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);
CREATE INDEX idx_product_inventory_id ON product (inventory_id);