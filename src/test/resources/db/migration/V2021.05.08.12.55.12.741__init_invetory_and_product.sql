insert into inventory(name,location) values
('Test 1', 'Location 1'),
('Test 2', 'Location 2'),
('Test 3', 'Location 3');

insert into product(type,price, description, inventory_id,name) values
('type 1',1.00, 'Description 1', (select id from inventory where name = 'Test 1'), 'Name 1'),
('type 2',2.00, 'Description 2', (select id from inventory where name = 'Test 1'), 'Name 2'),
('type 3',3.00, 'Description 3', (select id from inventory where name = 'Test 1'), 'Name 3'),

('type 1',4.00, 'Description 4', (select id from inventory where name = 'Test 2'), 'Name 4'),
('type 2',5.00, 'Description 5', (select id from inventory where name = 'Test 2'), 'Name 5'),
('type 3',6.00, 'Description 6', (select id from inventory where name = 'Test 2'), 'Name 6'),

('type 1',7.00, 'Description 7', (select id from inventory where name = 'Test 3'), 'Name 7'),
('type 2',8.00, 'Description 8', (select id from inventory where name = 'Test 3'), 'Name 8'),
('type 3',9.00, 'Description 9', (select id from inventory where name = 'Test 3'), 'Name 9');