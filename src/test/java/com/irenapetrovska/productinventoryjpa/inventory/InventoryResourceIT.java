package com.irenapetrovska.productinventoryjpa.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.flywaydb.core.Flyway;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InventoryResourceIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private Flyway flyway;

    @BeforeEach
    public void beforeEach(){
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void given_valid_request__when_create__then_inventory_created() throws Exception{
        var request = new InventoryRequest("name","Location");

        var responseRaw = mockMvc.perform(post("/api/inventories")
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var result = mapper.readValue(responseRaw, InventoryDTO.class);
        var expectedResult = new InventoryDTO(result.id, request.name, request.location);
        assertNotNull(result);
        assertEquals(expectedResult,result);

    }

    @Test
    public void when_find_page__then_return_page() throws Exception{
        var responseRaw = mockMvc.perform(get("/api/inventories")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        final var jsonObject = new JSONObject(responseRaw);
        final var contentJson = jsonObject.get("content");

        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, InventoryDTO.class);
        final List<InventoryDTO> result = mapper.readValue(contentJson.toString(), javaType);

        assertNotNull(result);
        assertEquals(
                List.of("Test 1","Test 2","Test 3"),
                result.stream()
                .map(inventory -> inventory.name)
                .collect(Collectors.toList())
        );
        assertEquals(
                List.of("Location 1","Location 2","Location 3"),
                result.stream()
                .map(inventory -> inventory.location)
                .collect(Collectors.toList())
        );


    }

}
