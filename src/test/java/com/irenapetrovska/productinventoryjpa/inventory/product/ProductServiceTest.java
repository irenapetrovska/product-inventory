package com.irenapetrovska.productinventoryjpa.inventory.product;

import com.irenapetrovska.productinventoryjpa.exception.ResourceNotFoundException;
import com.irenapetrovska.productinventoryjpa.inventory.Inventory;
import com.irenapetrovska.productinventoryjpa.inventory.InventoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private InventoryRepository inventoryRepository;
    @Mock
    private ProductRepository productRepository;

    @Test
    public void given_create_product_request__when_create_product__then_create_product(){
        when(inventoryRepository.findById(any()))
                .thenReturn(Optional.of(new Inventory()));
        when(productRepository.save(any(Product.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Product.class));
        var request = new ProductRequest("name","type", new BigDecimal(0),"Description");
        var expectedResult = new ProductDTO(null, request.name,request.type,request.price,request.description);
        var result = productService.create(0,request);

        assertNotNull(expectedResult);
        assertEquals(expectedResult,result);
    }
    @Test
    public void given_invalid_inventory_id__when_create_product__then_throw_exception(){
        when(inventoryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->  productService.create(0,new ProductRequest()));
    }

    @Test
    public void given_valid_product_id__when_update_product__then_update_product(){
        var request = new ProductRequest("name","type", new BigDecimal(0),"Description");
        var product = new Product(request.name,request.type,request.price,request.description);
        product.id=1;
        var expectedResult = product.toDTO();
        when(productRepository.findById(any()))
                .thenReturn(Optional.of(product));
        when(productRepository.save(any(Product.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Product.class));

        var result = productService.update(expectedResult.id,request);
        assertNotNull(result);
        assertEquals(expectedResult,result);
    }
    @Test
    public void given_invalid_product_id__when_update_product__then_throw_exception(){
        when(productRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->  productService.update(0,new ProductRequest()));
    }
    @Test
    public void given_valid_product_id__when_delete_product__then_delete_product(){
        var product = new Product();
        when(productRepository.findById(any()))
                .thenReturn(Optional.of(product));
        productService.delete(0);
        verify(productRepository, atLeastOnce())
                .delete(product);
    }
    @Test
    public void given_invalid_product_id__when_delete_product__then_throw_exception(){
        when(productRepository.findById(any()))
                .thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> productService.delete(0));
    }

}
