import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryComponent } from './inventory.component';
import { LayoutModule } from '../layout/layout.module';
import { InventoryRoutingModule } from './inventory.routing';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import { InventoryEditComponent } from './inventory-edit/inventory-edit.component';
import { InventoryService } from './inventory.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    InventoryListComponent,
    InventoryComponent,
    InventoryAddComponent,
    InventoryEditComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    LayoutModule,
    InventoryRoutingModule
  ],
  providers: [
    InventoryService
  ]
})
export class InventoryModule { }
