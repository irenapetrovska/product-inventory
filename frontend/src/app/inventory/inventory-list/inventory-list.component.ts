import { Component, OnInit } from '@angular/core';
import { Inventory } from '../inventory.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  inventories: Inventory[] = [];

  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    this.getPage(0, 10);
  }

  public onAddInventory(): void {
    console.log('Adding inventory ...');
  }

  private getPage(page: number, size: number) {
    this.inventoryService.getPageOfInventories(page, size).subscribe(
      successfulResponse => this.inventories = successfulResponse
      // error => console.log(error),
      // () => console.log('Complete')
    );
  }

}
