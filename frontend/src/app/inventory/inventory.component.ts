import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inventory',
  template: '<app-layout></app-layout>'
})
export class InventoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
