import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Inventory } from './inventory.domain';
import { catchError, map, tap } from 'rxjs/operators';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class InventoryService {

  // private httpClient: HttpClient;

  // constructor(httpClient: HttpClient) {
  //   this.httpClient = httpClient;
  // }

  constructor(private httpClient: HttpClient) {}

  public getPageOfInventories(page = 0, size = 10): Observable<Inventory[]> {
    const params = new HttpParams()
    .append('page', page)
    .append('size', size);
    return this.httpClient.get(`http://localhost:8089/api/inventories`, { params: params }).pipe(
      tap((page: any) => console.log(page)),
      map((page: any) => page.content),
      catchError(error => {
        console.error(error);
        // TODO show alert message!!!
        // return throwError(error);
        return of([]);
      })
    );
    // return of([{ id: 1, location: 'Skopje', name: 'some name' } as Inventory]);
  }

}
