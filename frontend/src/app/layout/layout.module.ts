import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CentralSectionComponent } from './central-section/central-section.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { LayoutComponent } from './layout.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    CentralSectionComponent,
    ContentComponent,
    FooterComponent,
    HeaderComponent,
    SidemenuComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule { }
